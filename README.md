# Install Instructions

```
npm install --save gitlab:probie1/ravello-shim
```

# Usage

The usage guide is different for http requests that require different information. 
You can check the [ravello api](https://www.ravellosystems.com/ravello-api-doc/) for an idea of what information is required.

All functions return promise with the response of ravello. This may be a java script object or a string depending on what ravello
returns, although the vast majority of functions return json. 

## When variable url parameters are required.

Provide a object mapping to the parameters.

```
const ravelloApi = require('ravello-shim');
let appPromise = ravelloApi.getApplication({ appId: ravelloAppId });
```

## When request data is required

Pass in the request data as the argument after the url parameter. 

```
const ravelloApi = require('ravello-shim');
let vmUpdatePromise = ravelloApi.updateVm({ appId: ravelloAppId, vmId: ravelloVmId }, ravelloVmManifest);
```

# Extending 

This library is made to be read and extended so seriously read the source (it's neither long nor particularly difficult). 

To add a new function use the base generator function and pass in the following information (any information you wish to defer
to call time simply supply the _ if you have lodash installed or ravelloApi._ if you do not).

urlTemplate: a lodash template that will resolve to the url you wish to use
method: ['GET', 'PUT' ...]
validation: A JOI object you wish to validate your input against (this is to catch potential ravello bugs with sending a request)
urlParams: The required params to compile the urlTemplate
data: Any request data
type: The type that ravello endpoint will return ['json', 'text']
