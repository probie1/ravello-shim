"use strict";

const request = require("superagent");
const _ = require("lodash");
const Joi = require("joi");
const Promise = require("bluebird");
const JSONbig = require("json-bigint");
const errors = require("./errors");

const BASE_URL = process.env["RAVELLO_SHIM"];

if (!BASE_URL) {
  throw Error("RAVELLO_SHIM must be defined");
}

function isJson(str) {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
}

let _request = function(
  urlTemplate,
  method,
  validation,
  urlParams,
  data,
  type
) {
  if (validation) {
    var result = Joi.validate(data, validation);

    if (result.error) {
      return Promise.reject(
        new errors.RavelloValidationError(result.error.toString())
      );
    }
  }

  let url = urlTemplate(urlParams);

  return new Promise((resolve, reject) => {
    request[method](`${BASE_URL}/${url}`)
      .type("json")
      .accept(type)
      .send(data)
      .end((err, res) => {
        if (err) {
          return reject(err);
        } else if (!res.text) {
          resolve(null);
        } else if (isJson(res.text)) {
          return resolve(JSONbig.parse(res.text));
        } else {
          return resolve(res.text);
        }
      });
  }).catch(err => {
    let url = urlTemplate(urlParams);
    if (
      !err.response ||
      !err.response.headers ||
      !err.response.headers["error-message"]
    ) {
      return Promise.reject(
        errors.RavelloError("Ravello request failed", url, err)
      );
    }
    let msg = err.response.headers["error-message"];
    return Promise.reject(new errors.RavelloError(msg, url, err));
  });
};

let base = (exports.base = _.curry(_request));
exports._ = _;
exports.errors = errors;

exports.createApplication = base(
  _.template("applications"),
  "post",
  null,
  null,
  _,
  "json"
);
exports.getApplication = base(
  _.template("applications/<%= appId %>"),
  "get",
  null,
  _,
  null,
  "json"
);
exports.addVm = base(
  _.template("applications/<%= appId %>/vms"),
  "post",
  null,
  _,
  _,
  "json"
);
exports.removeVm = base(
  _.template("applications/<%= appId %>/vms/<%= vmId %>"),
  "delete",
  null,
  _,
  null,
  "json"
);
exports.getVms = base(
  _.template("applications/<%= appId %>;design/vms"),
  "get",
  null,
  _,
  null,
  "json"
);
exports.getVm = base(
  _.template("applications/<%= appId %>;design/vms/<%= vmId %>"),
  "get",
  null,
  _,
  null,
  "json"
);
exports.vncUrl = base(
  _.template("applications/<%= appId %>/vms/<%= vmId %>/vncUrl"),
  "get",
  null,
  _,
  null,
  "text"
);
exports.updateVm = base(
  _.template("applications/<%= appId %>/vms/<%= vmId %>"),
  "put",
  null,
  _,
  _,
  "json"
);
exports.startAllVms = base(
  _.template("applications/<%= appId %>/start"),
  "post",
  null,
  _,
  null,
  "json"
);
exports.removeNetwork = base(
  _.template(
    "applications/<%= appId %>/network/services/networkInterfaces/<%= netId %>;deployment"
  ),
  "delete",
  null,
  _,
  null,
  "json"
);

exports.publishApplication = base(
  _.template("applications/<%= appId %>/publish"),
  "post",
  null,
  _,
  _,
  "json"
);
exports.publishApplicationUpdates = base(
  _.template("applications/<%= appId %>/publishUpdates?startAllDraftVms=false"),
  "post",
  null,
  _,
  null,
  "json"
);
exports.stopApplication = base(
  _.template("applications/<%= appId %>/stop"),
  "post",
  null,
  _,
  null,
  "json"
);

exports.stopVm = base(
  _.template("applications/<%= appId %>/vms/<%= vmId %>/stop"),
  "post",
  null,
  _,
  null,
  "json"
);
exports.startVm = base(
  _.template("applications/<%= appId %>/vms/<%= vmId %>/start"),
  "post",
  null,
  _,
  null,
  "json"
);
exports.powerOffVm = base(
  _.template("applications/<%= appId %>/vms/<%= vmId %>/poweroff"),
  "post",
  null,
  _,
  null,
  "json"
);
exports.redeployVm = base(
  _.template("applications/<%= appId %>/vms/<%= vmId %>/redeploy"),
  "post",
  null,
  _,
  null,
  "json"
);
exports.applicationDesignCost = base(
  _.template("applications/<%= appId %>/calcPrice;design"),
  "post",
  null,
  _,
  _,
  "json"
);
exports.applicationDeploymentCost = base(
  _.template("applications/<%= appId %>/calcPrice;deployment"),
  "post",
  null,
  _,
  _,
  "json"
);
exports.createBlueprint = base(
  _.template("blueprints"),
  "post",
  null,
  null,
  _,
  "json"
);
exports.deleteApplication = base(
  _.template("applications/<%= appId %>"),
  "delete",
  null,
  _,
  null,
  "json"
);
