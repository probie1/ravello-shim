'use strict';

module.exports = {
	RavelloError: RavelloError,
	RavelloValidationError: RavelloValidationError,
};


function RavelloError(message, request, parent, status) {
	this.name = 'Ravello Error';
	this.message = message || 'Ravello returned an error';
	this.request = request;
	this.statusCode = status;
	this.parent = parent;
	this.status = status || 400
	Error.captureStackTrace(this, RavelloError);
}
RavelloError.prototype = Object.create(Error.prototype);
RavelloError.prototype.constructor = RavelloError;


function RavelloValidationError(message, status) {
	this.name = 'Ravello Validation Error';
	this.message = message || 'Error validating ravello schema.';
	this.status = status || 400;
	Error.captureStackTrace(this, RavelloValidationError);
}
RavelloValidationError.prototype = Object.create(Error.prototype);
RavelloValidationError.prototype.constructor = RavelloValidationError;
